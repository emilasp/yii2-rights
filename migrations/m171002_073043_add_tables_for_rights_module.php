<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

/**
 * Class m171002_073043_add_tables_for_rights_module*/
class m171002_073043_add_tables_for_rights_module extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;

    /**
     * UP
     */
    public function up()
    {
        $this->createTable('rights_action', [
            'id'          => $this->primaryKey(11),
            'name'        => $this->string(255)->unique()->notNull(),
            'type'        => $this->smallInteger(1)->notNull(),
            'description' => $this->string(255),
        ], $this->tableOptions);

        $this->createTable('rights_group', [
            'id'          => $this->primaryKey(11),
            'name'        => $this->string(255)->notNull(),
            'description' => $this->string(255),
        ], $this->tableOptions);


        $this->createTable('rights_group_action_link', [
            'id'         => $this->primaryKey(11),
            'group_id'   => $this->integer(11)->notNull(),
            'action_id'  => $this->integer(11)->notNull(),
            'updated_at' => $this->dateTime(),
        ], $this->tableOptions);

        $this->createTable('rights_group_user_link', [
            'id'         => $this->primaryKey(11),
            'group_id'   => $this->integer(11)->notNull(),
            'user_id'    => $this->integer(11)->notNull(),
            'updated_at' => $this->dateTime(),
        ], $this->tableOptions);


        $this->addForeignKey(
            'fk_rights_group_action_link_group_id',
            'rights_group_action_link',
            'group_id',
            'rights_group',
            'id'
        );
        $this->addForeignKey(
            'fk_rights_group_action_link_action_id',
            'rights_group_action_link',
            'action_id',
            'rights_action',
            'id'
        );

        $this->addForeignKey(
            'fk_rights_group_user_link_group_id',
            'rights_group_user_link',
            'group_id',
            'rights_group',
            'id'
        );

        $this->addForeignKey(
            'fk_rights_group_user_link_user_id',
            'rights_group_user_link',
            'user_id',
            'users_user',
            'id'
        );

        $this->afterMigrate();
    }

    /**
     * DOWN
     */
    public function down()
    {
        $this->dropTable('rights_group_user_link');
        $this->dropTable('rights_group_action_link');
        $this->dropTable('rights_action');
        $this->dropTable('rights_group');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
