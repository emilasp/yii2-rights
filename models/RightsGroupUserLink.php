<?php

namespace emilasp\rights\models;

use backend\modules\users\models\User;
use emilasp\core\components\base\ActiveRecord;
use Yii;

/**
 * This is the model class for table "rights_group_user_link".
 *
 * @property int         $id
 * @property int         $group_id
 * @property int         $user_id
 * @property string      $updated_at
 *
 * @property RightsGroup $group
 * @property User        $user
 */
class RightsGroupUserLink extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rights_group_user_link';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'user_id'], 'required'],
            [['group_id', 'user_id'], 'default', 'value' => null],
            [['group_id', 'user_id'], 'integer'],
            [['updated_at'], 'safe'],
            [
                ['group_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => RightsGroup::className(),
                'targetAttribute' => ['group_id' => 'id']
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['user_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('site', 'ID'),
            'group_id'   => Yii::t('rights', 'Group ID'),
            'user_id'    => Yii::t('rights', 'User ID'),
            'updated_at' => Yii::t('site', 'Updated At'),
        ];
    }

    /** =======================================================================================
     * Relations Start
     * ======================================================================================= */
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(RightsGroup::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /** =======================================================================================
     * EVENTS
     * ======================================================================================= */
    /** =======================================================================================
     * CUSTOM METHODS
     * ======================================================================================= */
}
