<?php

namespace emilasp\rights\models;

use emilasp\core\components\base\ActiveRecord;
use Yii;

/**
 * This is the model class for table "rights_group".
 *
 * @property int                     $id
 * @property string                  $name
 * @property string                  $description
 *
 * @property RightsGroupActionLink[] $rightsGroupActionLinks
 * @property RightsGroupUserLink[]   $rightsGroupUserLinks
 * @property RightsAction[]          $rightsActions
 */
class RightsGroup extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rights_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'required'],
            [['name', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('site', 'ID'),
            'name'        => Yii::t('site', 'Name'),
            'description' => Yii::t('site', 'Description'),
        ];
    }

    /** =======================================================================================
     * Relations Start
     * ======================================================================================= */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRightsGroupActionLinks()
    {
        return $this->hasMany(RightsGroupActionLink::className(), ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRightsGroupUserLinks()
    {
        return $this->hasMany(RightsGroupUserLink::className(), ['group_id' => 'id']);
    }

    /**
     * @return $this
     */
    public function getRightsActions()
    {
        return $this->hasMany(RightsAction::className(), ['id' => 'action_id'])
            ->via('rightsGroupActionLinks');
    }
}
