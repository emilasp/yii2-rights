<?php

namespace emilasp\rights\models;

use emilasp\core\components\base\ActiveRecord;
use Yii;

/**
 * This is the model class for table "rights_group_action_link".
 *
 * @property int          $id
 * @property int          $group_id
 * @property int          $action_id
 * @property string       $updated_at
 *
 * @property RightsAction $action
 * @property RightsGroup  $group
 */
class RightsGroupActionLink extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rights_group_action_link';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'action_id'], 'required'],
            [['group_id', 'action_id'], 'default', 'value' => null],
            [['group_id', 'action_id'], 'integer'],
            [['updated_at'], 'safe'],
            [
                ['action_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => RightsAction::className(),
                'targetAttribute' => ['action_id' => 'id']
            ],
            [
                ['group_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => RightsGroup::className(),
                'targetAttribute' => ['group_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('site', 'ID'),
            'group_id'   => Yii::t('rights', 'Group ID'),
            'action_id'  => Yii::t('rights', 'Action ID'),
            'updated_at' => Yii::t('site', 'Updated At'),
        ];
    }

    /** =======================================================================================
     * Relations Start
     * ======================================================================================= */
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(RightsAction::className(), ['id' => 'action_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(RightsGroup::className(), ['id' => 'group_id']);
    }
    /** =======================================================================================
     * EVENTS
     * ======================================================================================= */
    /** =======================================================================================
     * CUSTOM METHODS
     * ======================================================================================= */
}
