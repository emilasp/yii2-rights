<?php

namespace emilasp\rights\models;

use emilasp\core\components\base\ActiveRecord;
use Yii;

/**
 * This is the model class for table "rights_action".
 *
 * @property int                     $id
 * @property string                  $name
 * @property int                     $type
 * @property string                  $description
 *
 * @property RightsGroupActionLink[] $rightsGroupActionLinks
 */
class RightsAction extends ActiveRecord
{

    public const TYPE_ROUTE  = 1;
    public const TYPE_SYSTEM = 2;
    public const TYPE_USER   = 3;

    /** @var array Types */
    public static $types = [
        self::TYPE_ROUTE  => 'route',
        self::TYPE_USER   => 'system',
        self::TYPE_SYSTEM => 'user',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rights_action';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'type'], 'required'],
            [['type'], 'integer'],
            [['name', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('site', 'ID'),
            'name'        => Yii::t('site', 'Name'),
            'type'        => Yii::t('site', 'Type'),
            'description' => Yii::t('site', 'Description'),
        ];
    }

    /** =======================================================================================
     * Relations Start
     * ======================================================================================= */
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRightsGroupActionLinks()
    {
        return $this->hasMany(RightsGroupActionLink::className(), ['action_id' => 'id']);
    }

    /** =======================================================================================
     * EVENTS
     * ======================================================================================= */
    /** =======================================================================================
     * CUSTOM METHODS
     * ======================================================================================= */
}
