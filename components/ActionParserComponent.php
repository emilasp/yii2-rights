<?php
namespace emilasp\rights\components;

use DirectoryIterator;
use Yii;
use yii\base\Component;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;

/**
 * Class ActionParserComponent
 * @package emilasp\rights\components
 */
class ActionParserComponent extends Component
{
    /**
     * Получаем структурированные экшены/действия
     *
     * @param string $folder
     * @return array
     */
    public function getActions(string $folder = '@app/modules')
    {
        return $this->getActionsAfterParse($folder);
    }

    /**
     * Получаем все контроллеры в дял модулей
     *
     * @param string $folder
     * @return array
     */
    private function getActionsAfterParse(string $folder): array
    {
        $actions = [];

        $path = Yii::getAlias($folder);

        if (is_dir($path)) {
            foreach (new DirectoryIterator($path) as $indexModule => $fileInfo) {
                if ($fileInfo->isDot()) {
                    continue;
                }

                if ($fileInfo->isDir()) {
                    $module         = $fileInfo->getBasename();
                    $controllerPath = $fileInfo->getRealPath() . '/controllers';

                    $actions[$indexModule] = ['name' => $module, 'path' => $controllerPath, 'controllers' => []];

                    if (is_dir($controllerPath)) {
                        $moduleControllers = FileHelper::findFiles($controllerPath, [
                            'filter' => function ($pathItem) {
                                return strpos($pathItem, 'Controller.php') !== false;
                            },
                        ]);

                        foreach ($moduleControllers as $indexController => $controller) {
                            $controllerName = str_replace('Controller.php', '', basename($controller));

                            $actions[$indexModule]['controllers'][$indexController] = [
                                'name'    => $controllerName,
                                'path'    => $controller,
                                'actions' => []
                            ];

                            $actionsTmp = $this->parseControllerActions($controller);

                            foreach ($actionsTmp as $indexAction => $actionTmp) {
                                $actions[$indexModule]['controllers'][$indexController]
                                ['actions'][$indexAction] = self::getActionNameByRoute(
                                    $module,
                                    $controllerName,
                                    $actionTmp
                                );
                            }
                        }
                    }
                }
            }
        }
        return $actions;
    }

    /**
     * Формируем действие из экшена
     *
     * @param string $module
     * @param string $controller
     * @param string $action
     * @return string
     */
    public static function getActionNameByRoute(string $module, string $controller, string $action): string
    {
        $module     = Inflector::camel2id($module) . DIRECTORY_SEPARATOR;
        $controller = Inflector::camel2id($controller) . DIRECTORY_SEPARATOR;
        $action     = Inflector::camel2id($action);

        return $module . $controller . $action;
    }

    /**
     * Получаем все экшены из контроллера
     *
     * @param string $pathController
     * @return array
     */
    private function parseControllerActions(string $pathController): array
    {
        preg_match_all("/function[ ]+action([\w]+)\(/", file_get_contents($pathController), $output);

        return $output[1];
    }
}
