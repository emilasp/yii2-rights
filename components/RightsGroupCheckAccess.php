<?php

namespace emilasp\rights\components;

use backend\modules\users\models\User;
use Yii;
use yii\helpers\ArrayHelper;
use yii\rbac\CheckAccessInterface;

/**
 * Компонент проверки прав
 *
 * Class RightsGroupCheckAccess
 * @package emilasp\rights\components
 */
class RightsGroupCheckAccess implements CheckAccessInterface
{
    const CACHE_ACTIONS_KEY_PREFIX = 'user:actions:';
    const CACHE_ACTIONS_DURATION   = 300;

    /**
     * Checks if the user has the specified permission.
     * @param string|int $userId the user ID. This should be either an integer or a string representing
     * the unique identifier of a user. See [[\yii\web\User::id]].
     * @param string     $permissionName the name of the permission to be checked against
     * @param array      $params name-value pairs that will be passed to the rules associated
     * with the roles and permissions assigned to the user.
     * @return bool whether the user has the specified permission.
     * @throws \yii\base\InvalidParamException if $permissionName does not refer to an existing permission
     */
    public function checkAccess($userId, $permissionName, $params = [])
    {
        $actions = $this->getUserActions();

        return array_search($permissionName, $actions) !== false;
    }

    /**
     * Получаем возможные для пользователя действия
     *
     * @return array
     */
    private function getUserActions(): array
    {
        /** @var User $user */
        $user = Yii::$app->user->identity;

        $actions = [];

        if (!Yii::$app->user->isGuest) {
            if (!$actions = Yii::$app->cache->get(self::CACHE_ACTIONS_KEY_PREFIX . $user->id)) {
                $groups = $user->rightsGroups;

                $actions = [];
                foreach ($groups as $group) {
                    $actions = ArrayHelper::merge($actions, $group->rightsActions);
                }

                $actions = ArrayHelper::getColumn($actions, 'name');

                Yii::$app->cache->set(self::CACHE_ACTIONS_KEY_PREFIX . $user->id, $actions, self::CACHE_ACTIONS_DURATION);
            }
            $actions[] = $user->role;
        }

        return $actions;
    }
}