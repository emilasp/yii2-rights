Модуль Rights для Yii2
=============================

Модуль прав с реализацией групп.

Installation
------------

Добавление нового модуля

Add to composer.json:
```json
"emilasp/yii2-rights": "*"
```
AND
```json
"repositories": [
        {
            "type": "git",
            "url":  "https://bitbucket.org/emilasp/yii2-rights.git"
        }
    ]
```

to the require section of your `composer.json` file.

Configuring
--------------------------

Добавляем :

```php
'modules' => [
        'rights' => []
    ],
```
