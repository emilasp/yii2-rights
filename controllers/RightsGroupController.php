<?php

namespace emilasp\rights\controllers;

use emilasp\rights\models\RightsGroupActionLink;
use emilasp\rights\models\RightsGroupUserLink;
use emilasp\rights\filters\AccessControl;
use Yii;
use emilasp\rights\models\RightsGroup;
use emilasp\rights\models\search\RightsGroupSearch;
use emilasp\core\components\base\Controller;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * RightsGroupController implements the CRUD actions for RightsGroup model.
 */
class RightsGroupController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index', 'view', 'create', 'update', 'delete', 'user-link', 'action-link'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'user-link', 'action-link'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete'      => ['POST'],
                    'user-link'   => ['POST'],
                    'action-link' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RightsGroup models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new RightsGroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RightsGroup model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, RightsGroup::className()),
        ]);
    }

    /**
     * Creates a new RightsGroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RightsGroup();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RightsGroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id, RightsGroup::className());

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RightsGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id, RightsGroup::className())->delete();

        return $this->redirect(['index']);
    }

    /**
     * Сохраняем связь action -> group
     *
     * @return array
     */
    public function actionActionLink(): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $groupId  = Yii::$app->request->post('groupId');
        $actionId = Yii::$app->request->post('actionId');
        $checked  = Yii::$app->request->post('checked');

        $model = RightsGroupActionLink::findOne(['group_id' => $groupId, 'action_id' => $actionId]);

        if ($checked && !$model) {
            $model = new RightsGroupActionLink(['group_id' => $groupId, 'action_id' => $actionId]);
            $model->save();
        } elseif (!$checked && $model) {
            $model->delete();
        }

        return $this->setAjaxResponse(self::AJAX_STATUS_SUCCESS, Yii::t('site', 'Good change link'));
    }

    /**
     * Сохраняем связь user -> group
     *
     * @return array
     */
    public function actionUserLink(): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $groupId = Yii::$app->request->post('groupId');
        $userId  = Yii::$app->request->post('userId');
        $checked = Yii::$app->request->post('checked');

        $model = RightsGroupUserLink::findOne(['group_id' => $groupId, 'user_id' => $userId]);

        if ($checked && !$model) {
            $model = new RightsGroupUserLink(['group_id' => $groupId, 'user_id' => $userId]);
            $model->save();
        } elseif (!$checked && $model) {
            $model->delete();
        }

        return $this->setAjaxResponse(self::AJAX_STATUS_SUCCESS, Yii::t('site', 'Good change link'));
    }
}
