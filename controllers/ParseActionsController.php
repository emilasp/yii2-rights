<?php

namespace emilasp\rights\controllers;

use emilasp\rights\components\ActionParserComponent;
use emilasp\rights\filters\AccessControl;
use emilasp\rights\models\RightsAction;
use Yii;
use emilasp\core\components\base\Controller;
use yii\helpers\ArrayHelper;

/**
 * ParseActionsController
 */
class ParseActionsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index', 'save'],
                'rules' => [
                    [
                        'actions' => ['index', 'save'],
                        'allow'   => true,
                        'roles'   => ['admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all RightsGroup models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'actions' => (new ActionParserComponent())->getActions(),
            'models'  => ArrayHelper::index(RightsAction::find()->all(), 'name'),
        ]);
    }

    /**
     * Save action
     * @return mixed
     */
    public function actionSave()
    {
        $id          = (int) Yii::$app->request->post('id');
        $name        = Yii::$app->request->post('name');
        $description = Yii::$app->request->post('description');

        if ((!$id || !$model = RightsAction::findOne($id)) && !$model = RightsAction::findOne(['name' => $name])) {
            $model = new RightsAction(['type' => RightsAction::TYPE_ROUTE]);
        }
        $model->name        = $name;
        $model->description = $description;
        $model->save();

        return $this->setAjaxResponse(self::AJAX_STATUS_SUCCESS, Yii::t('site', 'Update Success'));
    }
}
