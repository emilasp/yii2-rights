<?php

namespace emilasp\rights\filters;

use emilasp\rights\components\ActionParserComponent;
use Yii;
use yii\base\Action;
use yii\web\ForbiddenHttpException;

/**
 * Class AccessControl
 * @package emilasp\rights\filters
 */
class AccessControl extends \yii\filters\AccessControl
{
    /** @var array Роли для которых разрешен доступ всегда */
    public $godRoles = ['admin'];

    /**
     * @param Action $action
     * @return bool
     * @throws ForbiddenHttpException
     */
    public function beforeAction($action): bool
    {
        $access = false;

        if (parent::beforeAction($action)) {
            if ($this->only && array_search($action->id, $this->only) === false) {
                return true;
            }

            if ($this->cehckAccessRole($action)) {
                return true;
            }

            $action = ActionParserComponent::getActionNameByRoute(
                $action->controller->module->id,
                $action->controller->id,
                $action->id
            );

            $roles = $this->godRoles;
            $roles[] = $action;


            foreach ($roles as $permittion) {
                if (Yii::$app->user->can($permittion)) {
                    $access = true;
                    break;
                }
            }

            if (!$access) {
                throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
            }
        }

        return $access;
    }

    /**
     * Проверяем на роль
     *
     * @param Action $action
     * @return bool
     */
    private function cehckAccessRole(Action $action): bool
    {
        $role = Yii::$app->user->identity->role;

        foreach ($this->rules as $rule) {
            if (in_array($action->id, $rule->actions)) {
                $roles = $rule->roles ?? [];
                if (in_array($role, $roles)) {
                    return true;
                }
            }
        }

        return false;
    }
}
