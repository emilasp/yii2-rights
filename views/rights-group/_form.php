<?php

use emilasp\rights\models\RightsAction;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model emilasp\rights\models\RightsGroup */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="rights-group-form box box-primary">
        <?php $form = ActiveForm::begin(); ?>
        <div class="box-body table-responsive">

            <div class="row">
                <div class="col-md-6">

                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

                </div>
                <div class="col-md-6">

                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title"><?= Yii::t('rights', 'Actions') ?></h3>
                        </div>
                        <div class="panel-body">

                            <?php Pjax::begin(['id' => 'pjax-action-link-grid']); ?>


                            <?php if ($model->isNewRecord) : ?>
                                <h3 class="text-center"><?= Yii::t('site', 'Only after save model') ?></h3>
                            <?php else : ?>
                                <?= GridView::widget([
                                    'dataProvider' => new ActiveDataProvider([
                                        'query'      => RightsAction::find()->orderBy('type, name'),
                                        'pagination' => ['pageSize' => 0],
                                    ]),
                                    'columns'      => [
                                        ['class' => 'yii\grid\SerialColumn'],
                                        [
                                            'attribute' => 'name',
                                            'value'     => function ($modelEvent, $key, $index, $column) use ($model) {
                                                $class = 'primary';

                                                if ($modelEvent->type === RightsAction::TYPE_SYSTEM) {
                                                    $class = 'danger';
                                                } elseif ($modelEvent->type === RightsAction::TYPE_USER) {
                                                    $class = 'info';
                                                }

                                                $check = in_array(
                                                    $modelEvent->id,
                                                    ArrayHelper::getColumn($model->rightsGroupActionLinks, 'action_id')
                                                );

                                                if (!$check) {
                                                    $class = 'muted';
                                                }

                                                return Html::tag('i', $modelEvent->name, [
                                                    'class' => 'text-' . $class,
                                                ]);
                                            },
                                            'format'    => 'raw'
                                        ],
                                        [
                                            'attribute' => 'type',
                                            'value'     => function ($modelEvent, $key, $index, $column) {
                                                return RightsAction::$types[$modelEvent->type];
                                            },
                                        ],
                                        'description:ntext',
                                        [
                                            'label'     => '✔',
                                            'attribute' => 'name',
                                            'value'     => function ($modelEvent, $key, $index, $column) use ($model) {
                                                $check = in_array(
                                                    $modelEvent->id,
                                                    ArrayHelper::getColumn($model->rightsGroupActionLinks, 'action_id')
                                                );

                                                return Html::checkbox('selected', $check, [
                                                    'class'      => 'checkbox-action-link',
                                                    'data-group' => $model->id,
                                                    'value'      => $modelEvent->id,
                                                ]);
                                            },
                                            'format'    => 'raw'
                                        ],
                                    ],
                                    'tableOptions' => [
                                        'class' => 'table table-hover table-striped table-bordered'
                                    ],
                                    'rowOptions'   => function ($modelEvent, $key, $index, $column) use ($model) {
                                        $check = in_array(
                                            $modelEvent->id,
                                            ArrayHelper::getColumn($model->rightsGroupActionLinks, 'action_id')
                                        );

                                        if ($check) {
                                            return ['class' => 'text-success'];
                                        }
                                        return ['class' => 'text-muted'];
                                    },
                                ]); ?>
                            <?php endif; ?>

                            <?php Pjax::end(); ?>

                        </div>
                    </div>

                </div>
            </div>

        </div>
        <div class="box-footer text-right">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Save'),
                ['class' => 'btn btn-success btn-flat']
            ) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>


<?php
$urlToLinkAction = Url::toRoute('/rights/rights-group/action-link');

$js = <<<JS
   $(document).on('change', '.checkbox-action-link', function () {
       var actionId  = $(this).val();
       var groupId   = $(this).data('group');
       var checked   = $(this).is(':checked') ? 1 : 0;
       
       $.ajax({
            type: 'POST',
            url: '{$urlToLinkAction}',
            dataType: "json",
            data: 'actionId='+actionId + '&groupId=' + groupId + '&checked=' + checked,
            success: function(msg) {
                notice(msg['message'], (msg['status']=='1' ? 'green' : 'red'));
            }
       });
       
       
       
       $.pjax.reload({
           container:"#pjax-action-link-grid",
           "method":"POST",
           "timeout" : 0
       });
   });
JS;

$this->registerJs($js);