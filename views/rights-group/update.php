<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\rights\models\RightsGroup */

$this->title = Yii::t('site', 'Update {modelClass}: ', [
'modelClass' => Yii::t('rights', 'Group'),
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('rights', 'Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Updated');
?>
<div class="rights-group-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
