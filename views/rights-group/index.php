<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel emilasp\rights\models\search\RightsGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('rights', 'Groups');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rights-group-index box box-primary">

    <div class="box-header with-border text-right">
        <?= Html::a(Yii::t('site', 'Create'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>

    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'name',
                'description',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>

            <?php Pjax::end(); ?>

    </div>

</div>
