<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\rights\models\RightsGroup */

$this->title = Yii::t('site', 'Created');
$this->params['breadcrumbs'][] = ['label' => Yii::t('rights', 'Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rights-group-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
