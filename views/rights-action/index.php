<?php

use emilasp\rights\models\RightsAction;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\rights\models\search\RightsActionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('rights', 'Actions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rights-action-index box box-primary">

    <div class="box-header with-border text-right">
        <?= Html::a(Yii::t('site', 'Create'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>

    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?php Pjax::begin(); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'name',
                [
                    'attribute' => 'type',
                    'value'     => function ($model, $key, $index, $column) {
                        return RightsAction::$types[$model->type];
                    },
                    'class'     => '\yii\grid\DataColumn',
                    'filter'    => RightsAction::$types,
                ],
                'description',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>

        <?php Pjax::end(); ?>

    </div>

</div>
