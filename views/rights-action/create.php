<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\rights\models\RightsAction */

$this->title = Yii::t('site', 'Created');
$this->params['breadcrumbs'][] = ['label' => Yii::t('rights', 'Actions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rights-action-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
