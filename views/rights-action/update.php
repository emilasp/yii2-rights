<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\rights\models\RightsAction */

$this->title = Yii::t('site', 'Update {modelClass}: ', [
'modelClass' => Yii::t('rights', 'Action'),
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('rights', 'Actions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Updated');
?>
<div class="rights-action-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
