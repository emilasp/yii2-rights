<?php

use emilasp\rights\models\RightsAction;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\rights\models\RightsAction */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rights-action-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'type')->dropDownList(RightsAction::$types) ?>
            </div>
        </div>

    </div>
    <div class="box-footer text-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Save'),
                        ['class' => 'btn btn-success btn-flat']
            ) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
