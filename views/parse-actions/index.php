<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title                   = Yii::t('rights', 'Parse Actions');
$this->params['breadcrumbs'][] = $this->title;
?>


    <div class="user-view box box-primary">
        <div class="box-header text-right">

        </div>
        <div class="box-body table-responsive no-padding">

            <ul class="nav nav-tabs">
                <?php foreach ($actions as $index => $module) : ?>
                    <li class="<?= !$index ? 'active' : '' ?>">
                        <a data-toggle="tab" href="#<?= $module['name'] ?>"
                           class="nav-link <?= !$index ? 'active' : '' ?>">
                            <?= $module['name'] ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>

            <div class="tab-content">
                <?php foreach ($actions as $index => $module) : ?>
                    <div id="<?= $module['name'] ?>" class="tab-pane <?= !$index ? 'active' : 'fade' ?> clearfix">

                        <br/>

                        <div class="text-muted text-right"><?= $module['path'] ?></div>

                        <div class="row">
                            <div class="col-md-3">
                                <ul class="nav  nav-pills nav-stacked">
                                    <?php foreach ($module['controllers'] as $indexController => $controller) : ?>
                                        <li class="<?= !$indexController ? 'active' : '' ?>">
                                            <a data-toggle="tab" href="#<?= $controller['name'] ?>"
                                               class="nav-link <?= !$indexController ? 'active' : '' ?>">
                                                <?= $controller['name'] ?>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                            <div class="col-md-9">

                                <div class="tab-content">
                                    <?php foreach ($module['controllers'] as $indexController => $controller) : ?>
                                        <div id="<?= $controller['name'] ?>"
                                             class="tab-pane <?= !$indexController ? 'active' : 'fade' ?> clearfix">

                                            <div class="text-muted text-right"><?= $controller['path'] ?></div>

                                            <h2><?= $controller['name'] ?></h2>

                                            <table class="table table-hovered">
                                                <thead>
                                                <tr>
                                                    <th width="200px"><?= Yii::t('site', 'Name') ?></th>
                                                    <th><?= Yii::t('site', 'Description') ?></th>
                                                    <th width="100px">-</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <?php foreach ($controller['actions'] as $action) : ?>
                                                    <?
                                                    $hasModel    = isset($models[$action]);
                                                    $name        = $action;
                                                    $description = $hasModel ? $models[$action]->description : '';
                                                    $id          = $hasModel ? $models[$action]->id : '';
                                                    ?>
                                                    <tr class="<?= $hasModel ? 'success' : 'danger' ?>">
                                                        <td>
                                                            <?= Html::hiddenInput('actionId', $id, [
                                                                'class' => 'action-id'
                                                            ]) ?>
                                                            <?= Html::textInput('actionName', $name, [
                                                                'class' => 'form-control action-name'
                                                            ]) ?>
                                                        </td>
                                                        <td>
                                                            <?= Html::textarea('actionDescription', $description, [
                                                                'class' => 'form-control action-description'
                                                            ]) ?>
                                                        </td>
                                                        <td>
                                                            <?= Html::button(Yii::t('site', 'Save'), [
                                                                'class' => 'btn btn-success btn-save-action'
                                                            ]) ?>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>

                                                </tbody>
                                            </table>

                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>

                    </div>
                <?php endforeach; ?>
            </div>


        </div>
    </div>

<?php
$urlToSaveAction = Url::toRoute('/rights/parse-actions/save');
$js              = <<<JS
    $(document).on('change', '.btn-save-action', function() {
         var tr = $(this).closest('tr');
         tr.removeClass('danger').removeClass('success').removeClass('warning');
    });

    $(document).on('click', '.btn-save-action', function() {
        var tr = $(this).closest('tr');
        var id = tr.find('.action-id');
        var name = tr.find('.action-name');
        var description = tr.find('.action-description');
    
        if (name.val() === '' || description.val() === '') {
            notice('Has not empty', 'red');
            tr.addClass('danger').removeClass('success').removeClass('warning');
        } else {
            $.ajax({
                type: 'POST',
                url: '{$urlToSaveAction}',
                dataType: "json",
                data: 'id='+id.val() + '&name=' + name.val() + '&description=' + description.val(),
                success: function(msg) {
                    console.log(msg);
                    notice(msg['message'], (msg['status']=='1' ? 'green' : 'red'));
                    tr.addClass('success').removeClass('danger').removeClass('warning');
                }
            });
        }
    });
JS;

$this->registerJs($js);

