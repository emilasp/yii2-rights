<?php

namespace emilasp\rights;

use emilasp\core\CoreModule;

/**
 * Class RightsModule
 * @package backend\modules\users
 */
class RightsModule extends CoreModule
{
    public $defaultRoute        = 'rights';
    public $controllerNamespace = 'emilasp\rights\controllers';
}
