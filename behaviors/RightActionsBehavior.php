<?php
namespace emilasp\rights\behaviors;

use emilasp\rights\models\RightsGroup;
use emilasp\rights\models\RightsGroupUserLink;
use emilasp\core\components\base\ActiveRecord;
use yii;
use yii\base\Behavior;

/**
 * Class RightActionsBehavior
 * @package emilasp\rights\rights\behaviors
 */
class RightActionsBehavior extends Behavior
{
    /** @var  ActiveRecord */
    public $owner;

    /**
     * @return yii\db\ActiveQuery
     */
    public function getRightsGroupUserLinks()
    {
        return $this->owner->hasMany(RightsGroupUserLink::className(), ['user_id' => 'id']);
    }

    /**
     * @return mixed
     */
    public function getRightsGroups()
    {
        return $this->owner->hasMany(RightsGroup::className(), ['id' => 'group_id'])
            ->via('rightsGroupUserLinks')->with(['rightsActions']);
    }
}
